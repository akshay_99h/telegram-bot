require 'telegram_bot'

token = "1212706897:AAFWjNB1Fzy1qCZgSimLtBfwboSvzn3nG7s"

bot = TelegramBot.new(token: token)

bot.get_updates(fail_silently: true) do |message|
  puts "@#{message.from.username}: #{message.text}"
  command = message.get_command_for(bot)

  message.reply do |reply|
    case command
    when /start/i
      greetings = ['bonjour', 'hola', 'hallo', 'sveiki', 'namaste', 'salaam', 'szia', 'halo', 'ciao' , 'hi' , 'hello']
      reply.text = "#{greetings.sample.capitalize}, #{message.from.first_name}. You are here to join a quest. A journey through the internet. Try the /greet command."
    when /greet/i
      name = ['the encrypto' , 'the coder' , 'the finder' , 'the surfer' , 'the hacker']
      reply.text = "#{message.from.first_name}, #{name.sample}, along the way you will encounter various moments from your life. You will see things that will make you the happiest person alive. Type the /next command"
    when /next/i
      reply.text = "If you get stuck or need any help, you can always approach my master to get assistance. Type /link to get your first objective."
    when /link/i
      reply.text = "Follow this link: https://we.tl/t-9gc09z3Tpd (copy and paste in browser); (btw what are Ross and Chandler?)"
    else
      reply.text = "I have no idea what #{command.inspect} means."
    end
    puts "sending #{reply.text.inspect} to @#{message.from.username}"
    reply.send_with(bot)
  end
end